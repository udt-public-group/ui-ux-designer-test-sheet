# UI UX Designer Test Sheet

Please read this assignment carefully :) This assignment has a goal to assert your problem solving skill and your UX designing skill.
Therefore, you don't need complete this assignment 100%. Just try your best. We will rely on what you provide us and this test is used for wide range level which from fresher to a senior designer.

Please access ONE of the following websites: 
- https://app.livehomeroom.com
- https://www.okxe.vn
- https://stackshare.io/feed

1. you need to create a board in Figma
1. and create a simple design system follow Atomic Design framework.
1. you start re-designing the web.
1. let's make your result public that we an access without an account.
1. record a video by loom and describe your solution.

We will rely on your spending time. It means the more time you spent, the higher our expectation.

Thank you for your time,

and Happy designing 🥰
